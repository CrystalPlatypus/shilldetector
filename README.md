# Shill Detector

A plugin to help browsers identify news stories potentially affected by biased or corrupt reporting.

Both plugins built with Yeoman generators.

Chrome extension lives in ChromeExt. Built on ES6.

Firefox extension lives in FFExt. Built on ES5 (because an ES6 generator was not available and I am lazy). 

TODO:

Create AJAX behavior to retrieve shill list on plugin load - this will prevent users from needing to have an update every time a new name gets added to the list. Should still fall back to a static list in case of server failure, but AJAX seems to be the best way to go. Will need hosting of some sort.

